/* eslint-disable no-alert */
// eslint-disable-next-line no-unused-vars
async function routeClicked(id) {
  try {
    await fetch(`/${id}`, { method: 'GET' })
      .then((response) => response.text())
      .then((message) => {
        const fields = message.split(',');
        const price = fields[0].split(':');
        const type = fields[1].split(':');
        type[1] = type[1].substr(1, type[1].length - 4);
        document.getElementById(`rout_extraInfo_${id}`).innerText = `Ár: ${price[1]}, Típus: ${type[1]}`;
      });
  } catch (err) {
    window.alert(err.message);
    console.log(err.message);
  }
}

// eslint-disable-next-line no-unused-vars
async function userResClicked(jaratid, resid) {
  try {
    await fetch(`/userres/${jaratid}`, { method: 'GET' })
      .then((response) => response.text())
      .then((message) => {
        const fields = message.split(',');
        const from = fields[0].split(':');
        const to = fields[1].split(':');
        const date = fields[2].split(':');
        from[1] = from[1].substr(1, from[1].length - 2);
        to[1] = to[1].substr(1, to[1].length - 2);
        date[1] = date[1].substr(1, date[1].length - 4);
        document.getElementById(`resextra_${resid}`).innerText = `${from[1]} - ${to[1]} - ${date[1]}`;
      });
  } catch (err) {
    window.alert(err.message);
    console.log(err.message);
  }
}

// eslint-disable-next-line no-unused-vars
async function userGroupResClicked(jaratid, groupresid) {
  try {
    await fetch(`/userres/${jaratid}`, { method: 'GET' })
      .then((response) => response.text())
      .then((message) => {
        const fields = message.split(',');
        const from = fields[0].split(':');
        const to = fields[1].split(':');
        const date = fields[2].split(':');
        from[1] = from[1].substr(1, from[1].length - 2);
        to[1] = to[1].substr(1, to[1].length - 2);
        date[1] = date[1].substr(1, date[1].length - 4);
        document.getElementById(`resgextra_${groupresid}`).innerText = `${from[1]} - ${to[1]} - ${date[1]}`;
      });
  } catch (err) {
    window.alert(err.message);
    console.log(err.message);
  }
}

// eslint-disable-next-line no-unused-vars
async function deleteReservation(id, event) {
  const parent = event.target.parentNode;
  try {
    const deleteResult = await fetch(`/details/${id}`, { method: 'DELETE' });
    if (deleteResult.status === 204) {
      parent.remove();
    } else {
      throw new Error('Hiba torlesnel');
    }
  } catch (err) {
    window.alert(err.message);
    console.log(err.message);
  }
}

// eslint-disable-next-line no-unused-vars
async function deleteGReservation(id, event) {
  const parent = event.target.parentNode;
  try {
    const deleteResult = await fetch(`/dgroupres/${id}`, { method: 'DELETE' });
    if (deleteResult.status === 204) {
      parent.remove();
    } else {
      throw new Error('Hiba torlesnel innen1');
    }
  } catch (err) {
    window.alert(err.message);
    console.log(err.message);
  }
}

// eslint-disable-next-line no-unused-vars
function loginWindow() {
  const popup = document.getElementById('myPopup');
  popup.classList.toggle('show');
}
