import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import morgan from 'morgan';
import admin from './routes/admin.js';
import guest from './routes/guest.js';
import render from './routes/render.js';
import details from './routes/details.js';
import deleteRoute from './routes/delete_route.js';
import auth from './routes/auth.js';
import checkJWTToken from './middleware/authmiddleware.js';
import register from './routes/register.js';
import national from './routes/national.js';
import international from './routes/international.js';
import allroutes from './routes/allRoutes.js';
import dgroupres from './routes/deleteGroupRes.js';
import userpage from './routes/userpage.js';
import userres from './routes/userResInfo.js';

const app = express();
const staticdir = path.join(process.cwd(), 'static');
const PORT = process.env.PORT || 3000;

app.set('view engine', 'ejs');

app.use(morgan('tiny'));
app.use(cookieParser());
app.use(checkJWTToken);

app.use(express.static(staticdir));

app.use(express.urlencoded({ extended: true }));

app.use('/admin_adat_feltoltve', admin);
app.use('/vendeg_sikeres_kerese', guest);
app.use('/register_done', register);
app.use('/keres_belfoldi', national);
app.use('/keres_nemzetkozi', international);
app.use('/keres_minden', allroutes);

app.use('/userres', userres);
app.use('/userpage', userpage);
app.use('/dgroupres', dgroupres);
app.use('/details', details);
app.use('/', render);
app.use('/delete', deleteRoute);
app.use('/user_sikeres_foglalas', details);

app.use('/auth', auth);

app.listen(PORT, () => {
  console.log(`Server listening on port:${PORT}`);
});
