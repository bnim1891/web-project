import mysql from 'mysql';
import util from 'util';
import bcrypt from 'bcrypt';

const pool = mysql.createPool({
  connectionLimit: 10,
  database: 'webprog',
  host: 'localhost',
  port: 3306,
  user: 'root',
  password: 'idde2021',
});

const queryPromise = util.promisify(pool.query).bind(pool);

export function dropTables() {
  return queryPromise(`drop table if exists Jarat;
  drop table if exists Felhasznalo;
  drop table if exists Foglalas;`);
}

export function createRoutesTable() {
  return queryPromise(`create table if not exists Jarat(
    JaratID int auto_increment,
    honnan varchar(100),
    hova varchar(100),
    datum date,
    nap varchar(100),
    ido time,
    ar int,
    tipus varchar(100),
    orszagok varchar(100),
    primary key (JaratID));`);
}

export function createUserTable() {
  return queryPromise(`create table if not exists Felhasznalo(
        UserID int auto_increment,
        UserName varchar(100),
        UserEmail varchar(100),
        Password varchar(100),
        Rang int,
        primary key(UserID));`);
}

export function createReservationTable() {
  return queryPromise(`create table if not exists Foglalas(
        FoglalasID int auto_increment,
        JaratID int,
        UserID int,
        primary key(foglalasID),
        foreign key (JaratID) references Jarat(JaratID) on delete cascade,
        foreign key (UserID) references Felhasznalo(UserID));`);
}

export function createGroupReservationTable() {
  return queryPromise(`create table if not exists CsoportosFoglalas(
        CsoportosFoglalasID int auto_increment,
        JaratID int,
        UserID int,
        FelnottSzam int,
        GyerekSzam int,
        primary key(CsoportosFoglalasID),
        foreign key (JaratID) references Jarat(JaratID) on delete cascade,
        foreign key (UserID) references Felhasznalo(UserID));`);
}

export function insertCsoportosFoglalas(jaratid, userid, felnottdb, gyerekdb) {
  const query = 'insert into CsoportosFoglalas (JaratID, UserID, FelnottSzam, GyerekSzam) values (?, ?, ?, ?)';
  return queryPromise(query, [jaratid, userid, felnottdb, gyerekdb]);
}

export function selectCsoportosFoglalasok(jaratid) {
  const query = 'select * from CsoportosFoglalas where JaratID = ?';
  return queryPromise(query, [jaratid]);
}

export function selectUserCsFoglalas(userid) {
  const query = 'select * from CsoportosFoglalas where UserID = ?';
  return queryPromise(query, [userid]);
}

export function deleteCsFoglalas(csfoglalasid) {
  const query = 'delete from CsoportosFoglalas where CsoportosFoglalasID = ?;';
  return queryPromise(query, [csfoglalasid]);
}

export function selectCsFoglalasUserID(csfoglalasid) {
  const query = 'select UserID from CsoportosFoglalas where CsoportosFoglalasID = ?';
  return queryPromise(query, [csfoglalasid]);
}

export function insertJaratok(honnan, hova, datum, nap, ora, jegyar, vonattipus, belfoldi) {
  const query = 'insert into Jarat (honnan, hova, datum, nap, ido, ar, tipus, orszagok) values (?, ?, ?, ?, ?, ?, ?, ?)';
  return queryPromise(query, [honnan, hova, datum, nap, ora, jegyar, vonattipus, belfoldi]);
}

export function deleteJaratok(id) {
  const query = `delete from Jarat 
  where JaratID = ?;`;
  return queryPromise(query, [id]);
}

export function selectJaratok() {
  const query = 'select * from Jarat';
  return queryPromise(query, []);
}

export function selectJaratAr(jaratid) {
  const query = 'select ar from Jarat where JaratID = ?';
  return queryPromise(query, [jaratid]);
}

export function selectJaratInfo(jaratid) {
  const query = `select honnan, hova, datum from Jarat 
  where JaratID = ?;`;
  return queryPromise(query, [jaratid]);
}

export function selectJaratExtraInfo(id) {
  const query = 'select ar, tipus from Jarat where JaratID = ?;';
  return queryPromise(query, [id]);
}

export function selectJaratIds() {
  const query = 'select JaratID from Jarat';
  return queryPromise(query, []);
}

export function searchJaratok(honnan, hova, minar, maxar, datum) {
  const query = `select * from Jarat 
  where honnan = ? && hova = ? && ar >= ? && ar <= ? && datum = ?`;
  return queryPromise(query, [honnan, hova, minar, maxar, datum]);
}

export function nationalJaratok() {
  const query = 'select * from Jarat where orszagok = ? ';
  return queryPromise(query, ['belfoldi']);
}

export function internationalJaratok() {
  const query = 'select * from Jarat where orszagok = ? ';
  return queryPromise(query, ['nemzetkozi']);
}

export function insertUser(name, email, password, rang) {
  const query = `insert into Felhasznalo (UserName, UserEmail, Password, Rang) 
  values (?, ?, ?, ?)`;
  return queryPromise(query, [name, email, password, rang]);
}

export function selectUserIDs() {
  const query = `select UserID
  from Felhasznalo`;
  return queryPromise(query, []);
}

export function userNames() {
  const query = `select UserName 
  from Felhasznalo`;
  return queryPromise(query, []);
}

export function selectUserName(username) {
  const query = 'select UserName from Felhasznalo where UserName = ?';
  return queryPromise(query, [username]);
}

export function selectUserId(username) {
  const query = `select UserID
  from Felhasznalo where UserName = ?`;
  return queryPromise(query, [username]);
}

export function selectUserRang(username) {
  const query = `select Rang
  from Felhasznalo where UserName = ?`;
  return queryPromise(query, [username]);
}

export function selectPassword(username) {
  const query = `select Password
  from Felhasznalo where UserName = ?`;
  return queryPromise(query, [username]);
}

export function selectDeleteUserID(foglalasid) {
  const query = `select UserID 
  from Foglalas where FoglalasID = ?`;
  return queryPromise(query, [foglalasid]);
}

export function insertFoglalas(jaratid, userid) {
  const query = `insert into Foglalas (JaratID, UserID)
  values (?, ?)`;
  return queryPromise(query, [jaratid, userid]);
}

export function selectFoglalas(jaratid) {
  const query = `select * from Foglalas
  where JaratID = ?`;
  return queryPromise(query, [jaratid]);
}

export function deleteFoglalas(foglalasID) {
  const query = `delete from Foglalas
   where FoglalasID = ?`;
  return queryPromise(query, [foglalasID]);
}

export function selectUserFoglalas(userid) {
  const query = 'select * from Foglalas where UserID = ?';
  return queryPromise(query, [userid]);
}

createRoutesTable();
createUserTable();
createReservationTable();
createGroupReservationTable();

// eslint-disable-next-line no-unused-vars
export function insertPeople(username, email, password, rang) {
  const passwordHash = bcrypt.hashSync(password, 10);
  insertUser(username, email, passwordHash, rang);
}
