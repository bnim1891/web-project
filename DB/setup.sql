--mysql-t kell telepiteni
--az itt megadott kodreszlet inicializalja a mysql adatbazist
--a user (root) minden jogot megkap 

--MEGJELENITES:
--fooldal(kereso form + osszes jarat): http://localhost:3000
--user_setup.js futtatasa szukseges: letrehoz egy admint 
create DATABASE webprog if not exists;
select webprog;

ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'web2021'
flush privileges;