// adminformba bevezetett adatok validalasa
export  function validateAdminForm(req) {
  let errorStr = '';
  const regexCityName = new RegExp('^[a-zA-Z]+(?:[- ][a-zA-Z]+)*$');
  const regexNumber = new RegExp('^[0-9]*$');
  const regexTime = new RegExp('^[0-2][0-9]:[0-5][0-9]');
  if (!regexCityName.test(req.body.honnan) || !regexCityName.test(req.body.hova)) {
    errorStr += 'Hibás városnév \n';
  }
  if (req.body.datum === null) {
    errorStr += 'Hiba: datum \n';
  }
  if (!regexNumber.test(req.body.jegyar)) {
    errorStr += 'Hibás ár \n';
  }
  if (!regexTime.test(req.body.ora)) {
    errorStr += 'Hibás idő \n';
  }
  return errorStr;
}

export function validateAdminFormPart2(req) {
  let errorStr = '';
  const days = ['hetfo', 'kedd', 'szerda', 'csutortok', 'pentek'];
  const trainType = ['gyors', 'regionalis', 'szemely'];
  const travelType = ['belfoldi', 'nemzetkozi'];
  if (!days.includes(req.body.nap)) {
    errorStr += 'Hibás nap \n';
  }
  if (!trainType.includes(req.body.vonattipus)) {
    errorStr += 'Hibás vonattipus \n';
  }
  if (!travelType.includes(req.body.orszagok)) {
    errorStr += 'Hiba: utazas tipusa \n';
  }
  return errorStr;
}
