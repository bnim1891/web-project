import jwt from 'jsonwebtoken';
import secret from '../util/config.js';

export default async function checkJWTToken(req, res, next) {
  if (typeof (req.cookies) !== 'undefined') {
    if (req.cookies.token) {
      res.locals.payload = jwt.verify(req.cookies.token, secret);
      return next();
    }
    res.locals.payload = {};
    return next();
  }
  console.log('reg.cookies undefined');
  res.locals.payload = {};
  return next();
}
