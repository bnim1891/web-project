import { Router } from 'express';
import  { selectJaratok, selectJaratExtraInfo } from '../DB/db.js';

const router = Router();

// fooldal megjelenitese
router.get('/', async (req, res) => {
  const jaratok = await selectJaratok();
  const errorStr = '';
  console.log(jaratok);
  res.render('keres', { jaratok, errorStr });
});

// uj jarat bevezetesere alkalmas form megjelenitese
router.get('/admin', async (req, res) => {
  const errorStr = '';
  res.render('admin', { errorStr });
});

// regisztracios form megjelenitese
router.get('/register', async (req, res) => {
  const errorStr = '';
  res.render('register', { errorStr });
});

// extra info lekerese a kilistazott jaratokhoz
router.get('/:id', async (req, res) => {
  let jExtraInfo;
  try {
    jExtraInfo = await selectJaratExtraInfo(req.params.id);
  } catch (err) {
    res.status(500).send(`${err.name}: ${err.message}`);
  }
  res.json(jExtraInfo);
});

export default router;
