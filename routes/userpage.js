import { Router } from 'express';
import { selectUserCsFoglalas, selectUserFoglalas } from '../DB/db.js';

const router = Router();

// felhasznalo osszes foglalasanak megjelenitese
router.get('/', async (req, res) => {
  const foglalasok = await selectUserFoglalas(res.locals.payload.userid);
  const csoportfoglalasok = await selectUserCsFoglalas(res.locals.payload.userid);
  res.render('userpage', { foglalasok, csoportfoglalasok });
});

export default router;
