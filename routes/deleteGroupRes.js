import { Router } from 'express';
import { deleteCsFoglalas, selectCsFoglalasUserID } from '../DB/db.js';

const router = Router();
// csoportos foglalas torlese
router.delete('/:csfoglalasID', async (req, res) => {
  const { csfoglalasID } = req.params;
  const uid = await selectCsFoglalasUserID(csfoglalasID);
  if (uid[0].UserID === res.locals.payload.userid) {
    try {
      const retValue = await deleteCsFoglalas(csfoglalasID);
      if (retValue.affectedRows > 0) {
        res.status(204).end('Sikeres torles!');
      } else {
        throw new Error('Hiba torlesnel');
      }
    } catch (err) {
      res.status(500).send(`${err.name}: ${err.message}`);
    }
  } else {
    console.log('You are unauthorized');
    res.sendStatus(403);
  }
});

export default router;
