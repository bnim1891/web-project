import { Router } from 'express';
import { deleteJaratok } from '../DB/db.js';

const router = Router();
// jarat torlese utani informacio: sikeres/sikertelen torles
router.get('/:jaratID', async (req, res) => {
  const id = req.params.jaratID;
  try {
    const message = `${id} . járat törölve!`;
    await deleteJaratok(id);
    res.render('delete', { id, message });
  } catch (err) {
    const message = 'Sikertelen törlés!';
    res.render('delete', { id, message });
    console.log(err.message);
  }
});

export default router;
