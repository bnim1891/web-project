import { Router } from 'express';
import { selectJaratok } from '../DB/db.js';

const router = Router();

// osszes jarat megjelenitese a fooldalon
router.get('/', async (req, res) => {
  const jaratok = await selectJaratok();
  const errorStr = '';
  res.render('keres', { jaratok, errorStr });
});
export default router;
