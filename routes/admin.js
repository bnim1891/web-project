import { Router } from 'express';
import {
  insertJaratok,
  selectJaratok,
} from '../DB/db.js';

import { validateAdminForm, validateAdminFormPart2 } from '../middleware/adminFormValidate.js';

const router = Router();

router.post('/', async (req, res) => {
  let str1 = '';
  let str2 = '';
  str1 = validateAdminForm(req);
  str2 = validateAdminFormPart2(req);
  let errorStr = str1 + str2;
  if (errorStr !== '') {
    res.render('admin', { errorStr });
  } else {  // ha helyesek a bevezetett adatok
    await insertJaratok(req.body.honnan, req.body.hova, req.body.datum, req.body.nap,
      req.body.ora, req.body.jegyar, req.body.vonattipus, req.body.orszagok); // insert uj jaratot
    const jaratok = await selectJaratok();
    errorStr = '';
    res.render('keres', { errorStr, jaratok }); // vissza a fooldalra
  }
});
export default router;
