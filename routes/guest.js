import { Router } from 'express';
import {
  searchJaratok,
  selectJaratok,
} from '../DB/db.js';

const router = Router();

// kereses alapjan ujrarendereljuk a fooldalt a keresesnek megfelelo jaratokkal
router.get('/', async (req, res) => {
  const regexCityName = new RegExp('^[a-zA-Z]+(?:[- ][a-zA-Z]+)*$');
  const regexNumber = new RegExp('^[0-9]*$');
  let errorStr = '';
  if (!regexCityName.test(req.query.honnan)) {
    errorStr += 'Hibás kiindulási városnév \n';
  }
  if (!regexCityName.test(req.query.hova)) {
    errorStr += 'Hibás cél városnév \n';
  }
  if (!regexNumber.test(req.query.minar)) {
    errorStr += 'Hibás minimum ár \n';
  }
  if (!regexNumber.test(req.query.maxar)) {
    errorStr += 'Hibás maximum ár \n';
  }
  if (req.query.datum === '') {
    errorStr += 'Hiba: dátum \n';
  }
  if (errorStr !== '') {
    const jaratok = await selectJaratok();
    res.render('keres', { jaratok, errorStr }); // hiba eseten error uzenet
  } else {
    const jaratok = await searchJaratok(req.query.honnan, req.query.hova,
      req.query.minar, req.query.maxar, req.query.datum);
    res.render('keres', { jaratok, errorStr });
  }
});
export default router;
