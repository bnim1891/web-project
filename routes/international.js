import { Router } from 'express';
import { internationalJaratok } from '../DB/db.js';

const router = Router();

// nemzetkozi jaratok megjelenitese a fooldalon
router.get('/', async (req, res) => {
  const jaratok = await internationalJaratok();
  const errorStr = '';
  res.render('keres', { jaratok, errorStr });
});
export default router;
