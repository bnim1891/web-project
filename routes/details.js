import { Router } from 'express';
import {
  selectFoglalas, insertFoglalas, deleteFoglalas, selectDeleteUserID, selectJaratInfo,
  insertCsoportosFoglalas, selectCsoportosFoglalasok,
  selectJaratAr,
} from '../DB/db.js';

const router = Router();
// egy jarathoz tartozo ossze foglalas megjelenitese
router.get('/:jID', async (req, res) => {
  const id = req.params.jID;
  const foglalasok = await selectFoglalas(id);
  const jaratinfo = await selectJaratInfo(id);
  const csoportfoglalasok = await selectCsoportosFoglalasok(id);
  const ar = await selectJaratAr(id);
  const errorStr = '';
  res.render('reservations', {
    id, foglalasok, jaratinfo, ar, csoportfoglalasok, errorStr,
  });
});

// uj gyors foglalas
router.post('/:jaratID', async (req, res) => {
  const { userid } = res.locals.payload;
  const id = req.params.jaratID;
  await insertFoglalas(id, userid);
  const foglalasok = await selectFoglalas(id);
  const jaratinfo = await selectJaratInfo(id);
  const csoportfoglalasok = await selectCsoportosFoglalasok(id);
  const ar = await selectJaratAr(id);
  const errorStr = '';
  res.render('reservations', {
    id, foglalasok, jaratinfo, ar, csoportfoglalasok, errorStr,
  });
});

// uj csoportos foglalas
router.post('/group/:jaratID', async (req, res) => {
  const { userid } = res.locals.payload;
  const id = req.params.jaratID;
  let errorStr = '';
  const regexNumber = new RegExp('[0-9]*$');
  if (!regexNumber.test(req.body.felnottek)) {
    errorStr += 'Hiba: felnottek szama. \n';
  }
  if (!regexNumber.test(req.body.gyerekek)) {
    errorStr += 'Hiba: gyerekek szama. \n';
  }
  const foglalasok = await selectFoglalas(id);
  const jaratinfo = await selectJaratInfo(id);
  const ar = await selectJaratAr(id);
  if (errorStr !== '') {
    const csoportfoglalasok = await selectCsoportosFoglalasok(id);
    res.render('reservations', {
      id, foglalasok, jaratinfo, ar, csoportfoglalasok, errorStr,
    });
  } else {
    await insertCsoportosFoglalas(id, userid, req.body.felnottek, req.body.gyerekek);
    res.redirect(`/details/${id}`);
  }
});

// gyors foglalas torlese
router.delete('/:foglalasID', async (req, res) => {
  const { foglalasID } = req.params;
  const uid = await selectDeleteUserID(foglalasID);
  if (uid[0].UserID === res.locals.payload.userid) {
    try {
      const retValue = await deleteFoglalas(foglalasID);
      if (retValue.affectedRows > 0) {
        res.status(204).end('Sikeres torles!');
      } else {
        throw new Error('Hiba torlesnel');
      }
    } catch (err) {
      res.status(500).send(`${err.name}: ${err.message}`);
    }
  } else {
    console.log('You are unauthorized');
    res.sendStatus(403);
  }
});

export default router;
