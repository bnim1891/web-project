import { Router } from 'express';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import secret from '../util/config.js';
import {
  selectPassword, selectUserName, selectUserId, selectUserRang,
}
  from '../DB/db.js';

const router = Router();

router.post('/login', async (req, res) => {
  const { username, password } = req.body; // formba beirt username es password
  let ok = true;
  const dbUserName = await selectUserName(username);
  if (Object.keys(dbUserName).length === 0) {
    ok = false;
  }
  if (ok) {
    const dbP = await selectPassword(username);
    const id = await selectUserId(username);
    const rang = await selectUserRang(username);
    const userid = id[0].UserID;
    const userrang = rang[0].Rang;
    const dbPassword = dbP[0].Password;
    if (await bcrypt.compare(password, dbPassword)) {
      const token = jwt.sign({ username, userid, userrang }, secret);
      res.cookie('token', token, {
        httpOnly: true,
        sameSite: 'strict',
      });
      res.redirect('/');
    }
  }
  res.render('login_fail');
});

router.post('/logout', async (req, res) => {
  res.clearCookie('token');
  res.redirect('/');
});

export default router;
