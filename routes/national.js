import { Router } from 'express';
import { nationalJaratok } from '../DB/db.js';

const router = Router();

// belfoldi jaratok megjelenitese a fooldalon
router.get('/', async (req, res) => {
  const jaratok = await nationalJaratok();
  const errorStr = '';
  res.render('keres', { jaratok, errorStr });
});
export default router;
