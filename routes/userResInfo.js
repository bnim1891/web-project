import { Router } from 'express';
import { selectJaratInfo } from '../DB/db.js';

const router = Router();

// felhasznalo osszes foglalasahoz uj infok
router.get('/:id', async (req, res) => {
  let info;
  try {
    info = await selectJaratInfo(req.params.id);
  } catch (err) {
    res.status(500).send(`${err.name}: ${err.message}`);
  }
  res.json(info);
});

export default router;
