import { Router } from 'express';
import bcrypt from 'bcrypt';
import { insertUser, selectUserName } from '../DB/db.js';

const router = Router();
// regisztracios form kitoltese
router.post('/', async (req, res) => {
  let errorStr = '';
  const regexUserName =  new RegExp('[a-zA-Z0-9_]*$');
  const regexMail =  new RegExp('^[_a-z0-9-]+(.[a-z0-9-]+)@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$');
  const regexPassword = new RegExp('[a-zA-Z0-9]*$');
  if (!regexMail.test(req.body.email)) {
    errorStr += 'Hiba: email cim \n';
  }
  if (!regexUserName.test(req.body.username)) {
    errorStr += 'Hiba: felhasznalonev \n';
  }
  if (!regexPassword.test(req.body.jelszo_elso)) {
    errorStr += 'Jelszo: a beirt karakterek nem megengedettek. \n';
  }
  if (req.body.jelszo_elso !== req.body.jelszo_masodik) {
    errorStr += 'A megadott jelszavak nem egyeznek meg. \n';
  }
  const uname = await selectUserName(req.body.username);
  if (Object.keys(uname).length !== 0) {
    errorStr += 'A megadott felhasznalonev mar foglalt. \n';
  }
  if (errorStr !== '') {
    res.render('register', { errorStr }); // hiba eseten error uzenet
  } else {
    const passwordHash = bcrypt.hashSync(req.body.jelszo_elso, 10);
    // jelszo mentese hash-elt formaban
    await insertUser(req.body.username, req.body.email, passwordHash, 2);
    // uj felhasznalo becsurasa az adatbazisba
    res.redirect('/'); // vissza a fooldalra
  }
});

export default router;
